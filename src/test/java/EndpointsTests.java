import junit.framework.TestCase;

public class EndpointsTests extends TestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        // start server
        DataHandler dataHandler = new DataHandler();
        Endpoints endpoints = new Endpoints(dataHandler);
        endpoints.startServer();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testBooksEndpoint() {
        // endpoint /books
        // get all books in database
        String path = "books";
        APIUtils.TestResponse response = APIUtils.request("GET", path, null);
        String expectedBody = "[{\"title\":\"The Da Vinci Code\",\"author\":{\"name\":\"Dan Brown\",\"dateOfBirth\":{\"year\":1964,\"month\":6,\"day\":22},\"country\":\"United States of America\"},\"genre\":{\"name\":\"Thriller\"},\"isbn\":\"9780375432309\"},{\"title\":\"Programming for Dummies\",\"author\":{\"name\":\"Dan Gookin\",\"dateOfBirth\":{\"year\":1960,\"month\":10,\"day\":19},\"country\":\"United States of America\"},\"genre\":{\"name\":\"Technology\"},\"isbn\":\"9780470088708\"},{\"title\":\"Inferno\",\"author\":{\"name\":\"Dan Brown\",\"dateOfBirth\":{\"year\":1964,\"month\":6,\"day\":22},\"country\":\"United States of America\"},\"genre\":{\"name\":\"Thriller\"},\"isbn\":\"9788804631446\"}]";
        assertEquals(200, response.status);
        assertEquals(expectedBody, response.body);
    }

    public void testBookEndpoint() {
        // endpoint /books/0
        // get a single book with id 0
        String path = "books/0";
        APIUtils.TestResponse response = APIUtils.request("GET", path, null);
        String expectedBody = "{\"title\":\"The Da Vinci Code\",\"author\":{\"name\":\"Dan Brown\",\"dateOfBirth\":{\"year\":1964,\"month\":6,\"day\":22},\"country\":\"United States of America\"},\"genre\":{\"name\":\"Thriller\"},\"isbn\":\"9780375432309\"}";
        assertEquals(200, response.status);
        assertEquals(expectedBody, response.body);
    }

    public void testGenresEndpoint() {
        // endpoint /genres
        // get all genres in database
        String path = "genres";
        APIUtils.TestResponse response = APIUtils.request("GET", path, null);
        String expectedBody = "[{\"name\":\"Thriller\"},{\"name\":\"Technology\"}]";
        assertEquals(200, response.status);
        assertEquals(expectedBody, response.body);
    }

    public void testGenreEndpoint() {
        // endpoint /genres/0
        // get all genres in database
        String path = "genres/0";
        APIUtils.TestResponse response = APIUtils.request("GET", path, null);
        String expectedBody = "{\"name\":\"Thriller\"}";
        assertEquals(200, response.status);
        assertEquals(expectedBody, response.body);
    }

    public void testAuthorsEndpoint() {
        // endpoint /authors
        // get all authors in database
        String path = "authors";
        APIUtils.TestResponse response = APIUtils.request("GET", path, null);
        String expectedBody = "[{\"name\":\"Dan Brown\",\"dateOfBirth\":{\"year\":1964,\"month\":6,\"day\":22},\"country\":\"United States of America\"},{\"name\":\"Dan Gookin\",\"dateOfBirth\":{\"year\":1960,\"month\":10,\"day\":19},\"country\":\"United States of America\"}]";
        assertEquals(200, response.status);
        assertEquals(expectedBody, response.body);
    }

    public void testAuthorEndpoint() {
        // endpoint /authors/0
        // get a single author with id 0
        String path = "authors/0";
        APIUtils.TestResponse response = APIUtils.request("GET", path, null);
        String expectedBody = "{\"name\":\"Dan Brown\",\"dateOfBirth\":{\"year\":1964,\"month\":6,\"day\":22},\"country\":\"United States of America\"}";
        assertEquals(200, response.status);
        assertEquals(expectedBody, response.body);
    }

    public void testBooksForAuthorEndpoint() {
        // test endpoint author/0/books
        // get all books written by a single author
        String path = "authors/0/books";
        APIUtils.TestResponse response = APIUtils.request("GET", path, null);
        String expectedBody = "[{\"title\":\"The Da Vinci Code\",\"author\":{\"name\":\"Dan Brown\",\"dateOfBirth\":{\"year\":1964,\"month\":6,\"day\":22},\"country\":\"United States of America\"},\"genre\":{\"name\":\"Thriller\"},\"isbn\":\"9780375432309\"},{\"title\":\"Inferno\",\"author\":{\"name\":\"Dan Brown\",\"dateOfBirth\":{\"year\":1964,\"month\":6,\"day\":22},\"country\":\"United States of America\"},\"genre\":{\"name\":\"Thriller\"},\"isbn\":\"9788804631446\"}]";
        assertEquals(200, response.status);
        assertEquals(expectedBody, response.body);
    }

    public void testEndpointWithParameter() {
        // test endpoint /books?isbn={ISBN}
        // should return a book with a certain ISBN value
        String path = "books?isbn=9780470088708";
        APIUtils.TestResponse response = APIUtils.request("GET", path, null);
        String expectedBody = "{\"title\":\"Programming for Dummies\",\"author\":{\"name\":\"Dan Gookin\",\"dateOfBirth\":{\"year\":1960,\"month\":10,\"day\":19},\"country\":\"United States of America\"},\"genre\":{\"name\":\"Technology\"},\"isbn\":\"9780470088708\"}";
        assertEquals(200, response.status);
        assertEquals(expectedBody, response.body);
    }
}