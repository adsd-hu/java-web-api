import java.time.LocalDate;
import java.util.ArrayList;

public class DataHandler {

    private ArrayList<Book> books = new ArrayList<Book>();
    private ArrayList<Author> authors = new ArrayList<Author>();
    private ArrayList genres = new ArrayList<>();

    public DataHandler() {
        setupData();
    }

    private void setupData() {

        Author danBrown = new Author(
                "Dan Brown",
                LocalDate.of(1964,6,22),
                "United States of America");
        authors.add(danBrown);

        Genre thriller = new Genre("Thriller");
        genres.add(thriller);

        Book theDaVinciCode = new Book(
                "The Da Vinci Code",
                danBrown,
                thriller,
                "9780375432309");
        books.add(theDaVinciCode);

        Author danGookin = new Author(
                "Dan Gookin",
                LocalDate.of(1960,10,19),
                "United States of America");
        authors.add(danGookin);

        Genre technology = new Genre("Technology");
        genres.add(technology);

        Book programmingForDummies = new Book(
                "Programming for Dummies",
                danGookin,
                technology,
                "9780470088708");
        books.add(programmingForDummies);

        Book inferno = new Book(
                "Inferno",
                danBrown,
                thriller,
                "9788804631446");
        books.add(inferno);
    }

    public ArrayList<Book> getBooks() {
        return this.books;
    }

    public ArrayList<Author> getAuthors() {
        return this.authors;
    }

    public ArrayList<Genre> getGenres() {
        return this.genres;
    }
}