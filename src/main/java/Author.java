import java.time.LocalDate;

public class Author {
    private String name;
    private LocalDate dateOfBirth;
    private String country;

    public Author(String name, LocalDate dateOfBirth, String country) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.country = country;
    }

    public String getName() {
        return this.name;
    }

    public LocalDate getDateOfBirth() {
        return this.dateOfBirth;
    }

    public String getCountry() {
        return this.country;
    }
}