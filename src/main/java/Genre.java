public class Genre {
    private String name;

    Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
