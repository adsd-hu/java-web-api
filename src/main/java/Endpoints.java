import java.util.ArrayList;
import static spark.Spark.*;
import com.google.gson.Gson;

public class Endpoints {

    private DataHandler dataHandler;

    public Endpoints(DataHandler dataHandler) {
        this.dataHandler = dataHandler;
    }

    public void startServer() {

        final Gson gson = new Gson();
        ArrayList<Book> books = dataHandler.getBooks();
        ArrayList<Author> authors = dataHandler.getAuthors();
        ArrayList<Genre> genres = dataHandler.getGenres();

        port(getHerokuAssignedPort());

        get("/books", (req, res) -> {
            return gson.toJson(books);
        });

        get("/books/:id", (req, res) -> {
            Integer bookId = Integer.parseInt(req.params("id"));
            Book book = books.get(bookId);
            return gson.toJson(book);
        });

        after((req, res) -> {
            res.status(200);
            res.type("application/json");
        });

        notFound((req, res) -> {
            res.type("application/json");
            res.status(404);
            return "{\"message\":\"404 Not Found\"}";
        });
    }

    private static int getHerokuAssignedPort() {
        ProcessBuilder processBuilder = new ProcessBuilder();
        if (processBuilder.environment().get("PORT") != null) {
            return Integer.parseInt(processBuilder.environment().get("PORT"));
        }
        return 4567; //return default port if heroku-port isn't set (i.e. on localhost)
    }
}