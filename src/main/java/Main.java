public class Main {
    public static void main(String[] args) {

        DataHandler dataHandler = new DataHandler();
        Endpoints endpoints = new Endpoints(dataHandler);
        endpoints.startServer();
        System.out.println("Server running at http://localhost:4567");
    }
}
