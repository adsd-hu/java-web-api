public class Book {
    private String title;
    private Author author;
    private Genre genre;
    private String isbn;

    public Book(String title, Author author, Genre genre, String isbn) {
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.isbn = isbn;
    }

    public String getTitle() {
        return this.title;
    }

    public Author getAuthor() {
        return this.author;
    }

    public Genre getGenre() {
        return this.genre;
    }

    public String getISBN() {
        return this.isbn;
    }
}